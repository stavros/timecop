# timecop

A modern alternative to cron.

Timecop began mostly because I was a bit frustrated with cron's lack of features, especially for such a critical piece
of infrastructure, and none of the alternatives had everything I wanted.

Here's the list of features I want Timecop to eventually have:

* [X] Single, performant binary that you can install by copying it somewhere.
* [ ] Easy-to-use configuration files based on TOML, I don't want everything to fail because I added an extra asterisk.
* [ ] Configuration checking, I want to know right away if my config is wrong.
* [ ] Being able to force a task to run now under Timecop's user and environment, because I don't want to find out that
  an env var was wrong after I've left work.
* [ ] Ability to perform various actions on success or on failure.
* [ ] Ability for those actions to be sending emails, running commands or GETting HTTP endpoints.
* [ ] Fine-grained logging of stdout and stderr per-task.
* [ ] Command-line tools to tell me which jobs will run when.
* [ ] Command-line tools to tell me which jobs have run or have failed.
* [ ] Possibly add a verbal parser to convert "every other Monday at 9:30pm" to config lines.
* [ ] Ability to not run a task if the previous invocation is already running.
* [ ] Ability to pause/unpause tasks.

I think that's it for now, let's see how this goes.
